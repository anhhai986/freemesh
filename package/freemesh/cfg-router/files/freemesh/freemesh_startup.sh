#!/bin/sh
#run on startup each time

first_boot() {

	echo "`date` - starting first_boot()" >> /tmp/fmlog;
	#set wifi configs first time
	wlan2_mac="$(cat /sys/class/ieee80211/phy1/macaddress)"
	wlan5_mac="$(cat /sys/class/ieee80211/phy0/macaddress)"
	wlan2_nasid="$(echo $wlan2_mac|sed 's/://g')";
	wlan5_nasid="$(echo $wlan5_mac|sed 's/://g')";

	uci set wireless.ap_two.nasid="$wlan2_nasid"
	uci set wireless.ap_two.r1_key_holder="$wlan2_nasid"
	uci set wireless.ap_five.nasid="$wlan5_nasid"
	uci set wireless.ap_five.r1_key_holder="$wlan5_nasid"

	uci set wireless.mesh_five.mesh_id=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16`
	uci set wireless.mesh_five.key=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16`
	uci commit wireless;

	echo "`date` - done setting configs, setting symlink" >> /tmp/fmlog;
	ln -s /freemesh/www/handler.cgi /www/cgi-bin/handler.cgi;
	rm /www/luci-static/resources/view/network/wireless.js;
	ln -s /freemesh/www/wireless.js /www/luci-static/resources/view/network/wireless.js;

	uci set system.@system[0].hostname="fremesh";

	/freemesh/flashled.sh &

	uci set fm.router.firstboot=0;
	uci commit fm;
	echo "`date` - end of first_boot()" >> /tmp/fmlog;
}

normal_boot() {
	echo "`date` - normal_boot()" >> /tmp/fmlog;

	#for now.  The multi-colored lights on the latest look weird.
	echo "none" > /sys/class/leds/mt76-phy0/trigger;
	echo "none" > /sys/class/leds/mt76-phy1/trigger;

	echo "0" > /sys/class/leds/mt76-phy0/brightness;
	echo "0" > /sys/class/leds/mt76-phy1/brightness;

}

if [ `uci get fm.router.firstboot` == 1 ]; then
	first_boot;
else
	normal_boot;
fi




